﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="App_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <nav class="nav">
        <div class="nav-wrapper">
            <a href="Default.aspx" class="logo brand-logo">GAL-DEMO</a>
            <ul id="nav-mobile" class="right hide-on-med-and-down">
                <li><a href="Default.aspx">Inicio</a></li>
                <li><a href="empleado/Default.aspx">Empleados</a></li>
                <li><a href="#">Clientes</a></li>
                <li><a href="#">Proyectos</a></li>
            </ul>
        </div>
    </nav>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">

    <h1 class="header center">Maestros</h1>

    <div class="container">
        <div class="section">

            <div class="row">
                <div class="col s12 m7 l6">
                    <div class="card_menu card horizontal hoverable">
                        <div class="card-image">
                            <img class="img_car" src="images/icono-empleados.png">
                        </div>
                        <div class="card-stacked">
                            <div class="card-content">
                                <p>Gestionar la Información de Empleados</p>
                            </div>
                            <div class="card-action">
                                <a class="menu_link" href="empleado/Default.aspx">Empleados</a>
                            </div>
                        </div>
                    </div>
                </div>

                    <div class="col s12 m7 l6">
                    <div class="card_menu card horizontal hoverable">
                        <div class="card-image">
                            <img class="img_car" src="images/people.png">
                        </div>
                        <div class="card-stacked">
                            <div class="card-content">
                                <p>Gestionar la Información de Clientes</p>
                            </div>
                            <div class="card-action">
                                <a class="menu_link" href="#">Clientes</a>
                            </div>
                        </div>
                    </div>
                </div>

                 <div class="col s12 m7 l6">
                    <div class="card_menu card horizontal hoverable">
                        <div class="card-image">
                            <img class="img_car" src="images/project.png">
                        </div>
                        <div class="card-stacked">
                            <div class="card-content">
                                <p>Gestionar la Información de Proyectos</p>
                            </div>
                            <div class="card-action">
                                <a class="menu_link" href="#">Proyectos</a>
                            </div>
                        </div>
                    </div>
                </div>
            
            </div>

            
        </div>
    </div>

    </asp:Content>

