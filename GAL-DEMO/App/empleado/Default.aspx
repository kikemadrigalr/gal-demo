﻿<%@ Page Title="" Language="C#" MasterPageFile="~/App/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="App_empleado_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
   
    <nav class="nav">
        <div class="nav-wrapper">
            <a href="../Default.aspx" class="logo brand-logo">GAL-DEMO</a>
            <ul id="nav-mobile" class="right hide-on-med-and-down">
                <li><a href="../Default.aspx">Inicio</a></li>
                <li><a href="Default.aspx">Empleados</a></li>
                <li><a href="#">Clientes</a></li>
                <li><a href="#">Proyectos</a></li>
            </ul>
        </div>
    </nav>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    
    <div class="row">

      <div class="col s3">
        <!-- Grey navigation panel -->
          panel menu
      </div>

      <div class="col s9">
        <!-- Teal page content  -->
          panel
      </div>

    </div>

    <h2 class="header center titulos2">Empleados</h2>

    <div class="row">
       <div class="container">
        <div class="col s12 m12 l12">
            <a class="waves-effect waves-light btn-small"><i class="material-icons left">assignment_ind</i>Agregar Empleado</a>

            <table>
                <thead>
                    <tr>
                        <th>ID</th>
                        <th>Nombre</th>
                        <th>Cedula</th>
                        <th>Foto</th>
                        <th>Correo</th>
                        <th>Acción</th>
                    </tr>
                </thead>

                <tbody>
                    <tr>
                        <td>Alvin</td>
                        <td>Eclair</td>
                        <td>$0.87</td>
                    </tr>
                    <tr>
                        <td>Alan</td>
                        <td>Jellybean</td>
                        <td>$3.76</td>
                    </tr>
                    <tr>
                        <td>Jonathan</td>
                        <td>Lollipop</td>
                        <td>$7.00</td>
                    </tr>
                </tbody>
            </table>

        </div>
      </div>
    </div>
</asp:Content>

